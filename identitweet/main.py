import tweepy
import logging
import re
import requests
import tempfile
import requests
import os, sys
import snscrape.modules.twitter as sntwitter

from typing import List
from dotenv import load_dotenv
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from urllib.parse import urlparse

import identitweet.ocr as ocr
from identitweet.models.response_builder import ResponseBuilder

valid_extensions = ['.png', '.jpeg', '.jpg']

THRESHOLD = 0.2

class custom_streaming_client(tweepy.StreamingClient):
	def on_tweet(self, tweet):
		try:
			if (tweet.data['referenced_tweets'][0]['id']):
				url = determine_tweet(tweet.data['referenced_tweets'][0]['id'])['url']
				username = determine_tweet(tweet.data['referenced_tweets'][0]['id'])['url']
				print(url)
				if url is not None:
					client.create_tweet(text='✅ Found! ' + str(url), in_reply_to_tweet_id=tweet.id)
				elif username is not None:
					client.create_tweet(text='❌ Identitweet couldn\'t find your tweet. The tweet either does not exist or its text was not identifiable.', in_reply_to_tweet_id=tweet.id)
				else:
					client.create_tweet(text='❌ Identitweet couldn\'t find your tweet. The username in the tweet was not identifiable', in_reply_to_tweet_id=tweet.id)
			else:
				pass
		except Exception as e:
			print(e)

def load_twitter():
	load_dotenv()
	global client, streaming_client
	bearer_token = os.environ.get('BEARER_TOKEN')
	consumer_key = os.environ.get('CONSUMER_KEY')
	consumer_key_secret = os.environ.get('CONSUMER_KEY_SECRET')
	access_token = os.environ.get('ACCESS_TOKEN')
	access_token_secret = os.environ.get('ACCESS_TOKEN_SECRET')
	client = tweepy.Client(bearer_token, consumer_key, consumer_key_secret, access_token, access_token_secret)
	streaming_client = custom_streaming_client(bearer_token)

def init_stream():
	streaming_client.add_rules(tweepy.StreamRule('@identitweetbot -from:identitweetbot'))
	streaming_client.filter(expansions=['referenced_tweets.id'])

def get_tweet_image(tweet_id):
	tweet = client.get_tweet(id=tweet_id, expansions=['attachments.media_keys'], media_fields=['url'])
	images = []
	for i in range(len(tweet.includes['media'])):
		images.append(tweet.includes['media'][i].url)
	return images

def requests_retry_session(
		retries = 3,
		backoff_factor = 2,
		status_forcelist = (500, 502, 504),
		session = None,
	):
		session = session or requests.Session()
		retry = Retry(
			total = retries,
			read = retries,
			connect = retries,
			backoff_factor = backoff_factor,
			status_forcelist = status_forcelist
		)
		adapter = HTTPAdapter(max_retries = retry)
		session.mount('http://', adapter)
		session.mount('https://', adapter)
		return session

def get_file_name(url):
	parsed = urlparse(url)
	if parsed.scheme != 'https':
		return None
	path = parsed.path
	index = path.rfind('/')
	if index == -1:
		index = path.rfind('\\')
	filename = path[index+1:]
	thing = filename.find('?')
	if thing != -1:
		filename = filename[:thing]
	return filename

def get_text_from_file_name(path: str, filename: str) -> List[str]:
	text = ocr.get_text_from_path(path, filename)
	text = text.lower()
	array = [char for char in text]
	if len(sys.argv) > 1:
		logging.info(' '.join(array))
		logging.info('==============')
	return array

def valid_image(url):
	filename = get_file_name(url)
	if filename is None:
		return False
	print(url + ' -> ' + filename)
	for ext in valid_extensions:
		if filename.endswith(ext):
			return True
	return False

def determine_tweet(tweet_id):
	urls = get_tweet_image(tweet_id)
	ocr_urls = [x for x in urls if valid_image(x)]
	ocr_array = []
	builder = None
	for url in ocr_urls:
		wordArray = handle_url(url)
		if wordArray is None or len(wordArray) == 0:
			continue

		if builder is None:
			builder = ResponseBuilder(THRESHOLD)
			builder.RecognizedText = ''.join(wordArray)
			builder.FormattedText = '>' + builder.RecognizedText.replace('\n', '\n>')
		else:
			text = ' '.join(wordArray)
			builder.RecognizedText += '\r\n---\r\n' + text
			builder.FormattedText += '\r\n---\r\n' + text.replace('\n', '\n>')

		ocr_array.extend(wordArray)
	text = builder.RecognizedText.replace('\n', ' ').replace('\r', '').replace('|', 'l').replace("@ ", "", 1)
	username_match = re.search('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9-_]+)', text)
	tweet_length = len(text)
	truncated_tweet = text[tweet_length:tweet_length * 2]
	if (username_match is not None):
		username = username_match.group()
		if (re.search('((?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9-_]+)?).*', text)) is not None:
				truncated_tweet = re.search('((?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9-_]+)?).*', text).group()
				truncated_tweet = truncated_tweet[len(username):].strip().split()
				truncated_tweet = ' '.join(truncated_tweet[:5])
		if (re.search('[0-9]+\/[0-9]+\/[0-9]+', truncated_tweet)) is not None:
				truncated_tweet = re.sub('[0-9]+\/[0-9]+\/[0-9]+', '', truncated_tweet)
		username = username.split('-', 1)[0]
		print("Username: " + username + "\n")
		print("Tweet: " + truncated_tweet + "\n")
	try:
		tweet = list(sntwitter.TwitterSearchScraper('from:' + username + ' ' + truncated_tweet).get_items())[0]
	except:
		tweet = None
	try:
		client.get_user(username=username[1:]).errors[0]['title']
	except:
		pass
	else:
		username = None
	return {'username': username, 'text': truncated_tweet, 'url': tweet}


def handle_url(url: str) -> List[str]:
	filename = get_file_name(url)
	try:
		r = requests_retry_session(retries = 5).get(url)
	except Exception as x:
		logging.error('Couldn\'t handle URL: {0} {1}'.format(url, x.__class__.__name__))
		return
	if not r.ok:
		print('=== err')
		print(url)
		print(r)
		print('===')
		return
	tempPath = os.path.join(tempfile.gettempdir(), filename)
	print(tempPath)
	with open(tempPath, 'wb') as f:
		f.write(r.content)
	return get_text_from_file_name(tempPath, filename)

def start():
	load_twitter()
	init_stream()