import logging
try:
	from PIL import Image
except ImportError:
	import Image
import argparse
import pytesseract
import cv2
import os, tempfile
if os.name == 'nt':
	pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'

def get_text_from_path(path, filename):
	image = cv2.imread(path, cv2.IMREAD_COLOR)
	image = cv2.addWeighted(image, 0.25, image, 0, 100)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	gray = cv2.bitwise_not(gray)
	filename = "corrected_{}.png".format(filename)
	corrected_path = os.path.join(tempfile.gettempdir(), filename)
	cv2.imwrite(corrected_path, gray)
	return pytesseract.image_to_string(image = Image.open(corrected_path))