import os, re
from typing import List
from json import JSONEncoder

class ResponseBuilder:
	def __init__(self, threshold):
		self.Threshold = threshold
		self.RecognizedText = ""
		self.FormattedText = ""