# Identitweet
the repository for [@identitweetbot](https://twitter.com/identitweetbot).

to run, first install the dependencies in `requirements.txt`. [tesseract-ocr](https://github.com/tesseract-ocr/tesseract) is required to run this project. then, create a `.env` file in the root of the file containing the following:

```
BEARER_TOKEN=
CONSUMER_KEY=
CONSUMER_KEY_SECRET=
ACCESS_TOKEN=
ACCESS_TOKEN_SECRET=
```

the appropriate tokens and keys can be obtained by [creating a Twitter app](https://developer.twitter.com/en/apps/create). from there, run `run.py` and the bot should be online.

## Overview
@identitweetbot works by first monitoring all tweets that directly mention it. the bot filters only replies.

if the reply has an image, the bot downloads the image to the system's temporary directory and runs various post-processing filters on the image, including blending the colors with a weight of 0.25 and setting the image to grayscale.

from there, the bot runs the corrected image through tesseract. using some *RegEx magic*, the bot will attempt to grab the @ and the text of the tweet, and runs both through Twitter's search. if the bot locates the tweet, it replies to the initial reply with the tweet link; otherwise, it replies with an error message.

## Limitations
identitweet cannot do the following:
* handle tweets that do not contain a discernable @ symbol
* handle tweets containing languages other than English, although the language of the text detection can be changed (at the expense of other languages)
* find deleted tweets, obviously
